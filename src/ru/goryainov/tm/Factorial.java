package ru.goryainov.tm;

import java.math.BigInteger;

public class Factorial implements IFactorial {

    @Override
    public BigInteger calculateFactorial(Integer n) {
        BigInteger ret = BigInteger.ONE;
        for (int i = 1; i <= n; ++i) ret = ret.multiply(BigInteger.valueOf(i));
        return ret;
    }
}
