package ru.goryainov.tm;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

public class FactorialHandler implements InvocationHandler {
    Logger logger = Logger.getLogger(FactorialHandler.class.getName());
    Factorial factorial;
    Map<Integer, BigInteger> factorialCache = new HashMap<>();
    public FactorialHandler(Factorial factorial) {
        this.factorial = factorial;
    }


    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        if (method.getName().equals("calculateFactorial")) {
            Integer number = (Integer) args[0];
            if (factorialCache.containsKey(number)) {
                logger.info("Factorial of a number from the cache");
                return factorialCache.get(number);
            }
            BigInteger value = (BigInteger) method.invoke(factorial, args);
            factorialCache.put(number, value);
            return value;
        }
        return method.invoke(factorial, args);

    }
}
