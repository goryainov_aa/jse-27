package ru.goryainov.tm;

import java.math.BigInteger;

public interface IFactorial {
    BigInteger calculateFactorial(Integer n);
}
