package ru.goryainov.tm;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Proxy;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {

    static Logger logger = Logger.getLogger(Main.class.getName());

    public static void main(String[] args) {

        Factorial factorial = new Factorial();
        FactorialHandler factorialHandler = new FactorialHandler(factorial);
        IFactorial proxy = (IFactorial) Proxy.newProxyInstance(factorial.getClass().getClassLoader(), factorial.getClass().getInterfaces(), factorialHandler);
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("WELCOME");
        while (true) {
            System.out.println("Enter int 'n' for calculate factorial");
            String val = null;
            try {
                 val = bufferedReader.readLine();
            } catch (IOException e) {
                logger.log(Level.SEVERE, "Value is empty!");
            }
            if (val.equals("exit")) {
                logger.log(Level.INFO, "Program exit");
                break;
            }
            Integer n = Integer.parseInt(val);
            if (n < 0) {
                logger.log(Level.SEVERE, "Error! N is negative.");
            }
            System.out.println(proxy.calculateFactorial(n));
        }

    }
}
